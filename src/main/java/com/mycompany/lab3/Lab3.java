/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab3;

/**
 *
 * @author Tauru
 */
public class Lab3 {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

    static int add(int num1, int num2) {
        return num1 + num2;
        }

    static boolean checkWin(String[][] table, String playerTurn) {
        if(checkRow(table,playerTurn)){
            return true;
    }
        if(checkCol(table,playerTurn)){
            return true;
    }
        if(checkDiagonals(table,playerTurn)){
            return true;
        }
        return false;
    }

    private static boolean checkRow(String[][] table, String playerTurn) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0].equals(table[i][1]) && table[i][0].equals(table[i][2])) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] table, String playerTurn) {
        for (int j = 0; j < 3; j++) {
            if (table[0][j].equals(table[1][j]) && table[0][j].equals(table[2][j])) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkDiagonals(String[][] table, String playerTurn) {
        if ((table[0][0].equals(table[1][1]) && table[0][0].equals(table[2][2]))
                || (table[0][2].equals(table[1][1]) && table[0][2].equals(table[2][0]))) {
            return true;
        }
        return false;
    }
    
    static boolean checkDraw(String[][] table, String playerTurn) {
        if(checkTable(table,playerTurn)){
            return true;
    }
    
        return false;
    }

    private static boolean checkTable(String[][] table, String playerTurn) {
        for(int i = 0;i<3;i++) {
            for (int j = 0; j < 3; j++) {
                if (!table[i][j].equals("X") && !table[i][j].equals("O")) {
                    return false;
                }
            }
        }
        return true;
    }
}
