package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Tauru
 */
public class OXUnitTest {
    
    public OXUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testCheckWin_X_Horizontal1_output_true(){
        String[][] table = {{"X", "X", "X"},
    {"4", "5", "6"},
    {"1", "2", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal2_output_true(){
        String[][] table = {{"7", "8", "9"},
    {"X", "X", "X"},
    {"1", "2", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal3_output_true(){
        String[][] table = {{"7", "8", "9"},
    {"4", "5", "6"},
    {"X", "X", "X"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal1_output_false(){
        String[][] table = {{"X", "X", "9"},
    {"4", "5", "6"},
    {"1", "2", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal2_output_false(){
        String[][] table = {{"7", "8", "9"},
    {"X", "X", "6"},
    {"1", "2", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    @Test
    public void testCheckWin_X_Horizontal3_output_false(){
        String[][] table = {{"7", "8", "9"},
    {"4", "5", "6"},
    {"X", "X", "3"}
    };
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    @Test
    public void testCheckWin_X_Vertical1_output_true(){
        String[][] table = {{"X", "8", "9"},
    {"X", "5", "6"},
    {"X", "2", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Vertical2_output_true(){
        String[][] table = {{"7", "X", "9"},
    {"4", "X", "6"},
    {"1", "X", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Vertical3_output_true(){
        String[][] table = {{"7", "8", "X"},
    {"4", "5", "X"},
    {"1", "2", "X"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Vertical1_output_false(){
        String[][] table = {{"X", "8", "9"},
    {"X", "5", "6"},
    {"1", "2", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    @Test
    public void testCheckWin_X_Vertical2_output_false(){
        String[][] table = {{"7", "X", "9"},
    {"4", "X", "6"},
    {"1", "2", "3"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    @Test
    public void testCheckWin_X_Vertical3_output_false(){
        String[][] table = {{"7", "X", "9"},
    {"4", "5", "X"},
    {"1", "2", "X"}
    };  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    @Test
    public void testCheckWin_X_Diagonals1_output_true(){
        String[][] table = {{"X", "8", "9"},
                            {"4", "X", "6"},
                            {"1", "2", "X"}};  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Diagonals2_output_true(){
        String[][] table = {{"7", "8", "X"},
                            {"4", "X", "6"},
                            {"X", "2", "3"}};  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(true,result);
    }
    
    @Test
    public void testCheckWin_X_Diagonals1_output_false(){
        String[][] table = {{"7", "8", "9"},
                            {"4", "X", "6"},
                            {"1", "2", "X"}};  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    @Test
    public void testCheckWin_X_Diagonals2_output_false(){
        String[][] table = {{"7", "8", "X"},
                            {"4", "X", "6"},
                            {"1", "X", "3"}};  
       String playerTurn = "X";
       boolean result = Lab3.checkWin(table,playerTurn);
       assertEquals(false,result);
    }
    
    
    @Test
    public void testCheckDraw_X_O_output_true(){
        String[][] table = {{"X", "O", "X"},
                            {"O", "X", "O"},
                            {"O", "X", "O"}};  
       String playerTurn = "X";
       boolean result = Lab3.checkDraw(table,playerTurn);
       assertEquals(true,result);
    }
}
